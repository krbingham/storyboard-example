# storybook

## Project setup
```
npm install
```

### Storyboard build
````
npm run storyboard:build
````

## Story location
You can find the stories here with an example of the hello world provided by vue
```
./src/stories
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Storyboard setup with vue-cli
See [Vue Cli Plyugin Storybook](https://github.com/storybookjs/vue-cli-plugin-storybook)

### Storyboard quick start guide
See [Vue Storybook quickstart guide](https://storybook.js.org/docs/guides/quick-start-guide/)