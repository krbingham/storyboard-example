import HelloWorld from "@/components/HelloWorld.vue";

export default { title: "Hello World" };

export const asAComponent = () => ({
  components: { HelloWorld },
  template: '<hello-world msg="Welcome to Your Vue.js App"></hello-world>'
});
